from functools import partial

from Tabras.battle import Battle
from Tabras.characters import AICharacter, PlayerCharacter


def dummy_print_battle(battle, character):
    mapping = {
        PlayerCharacter: 'P',
        AICharacter: 'A',
        type(None): '.',
    }
    size = battle.battlefield.size
    fields = [
        ['#' for _ in range(size)]
        for _ in range(size)
    ]
    for field in battle.battlefield.visible_fields(character):
        x, y = field.x, field.y
        fields[y][x] = mapping[type(field.content)]

    for row in fields:
        print(''.join(row))


def move_left(battle, character):
    new_field = battle.fields.left(character.field)
    if new_field is not None:
        battle._move(character, new_field)


def move_right(battle, character):
    new_field = battle.fields.right(character.field)
    if new_field is not None:
        battle._move(character, new_field)


def move_up(battle, character):
    new_field = battle.fields.upper(character.field)
    if new_field is not None:
        battle._move(character, new_field)


def move_down(battle, character):
    new_field = battle.fields.lower(character.field)
    if new_field is not None:
        battle._move(character, new_field)


if __name__ == '__main__':
    battle = Battle()
    character = battle.groups[0].characters[0]
    enemy = battle.groups[1].characters[0]
    key_mapping = {
        'a': partial(move_left, battle, character),
        'd': partial(move_right, battle, character),
        'w': partial(move_up, battle, character),
        's': partial(move_down, battle, character),
        ' ': partial(character.attack, enemy)
    }

    while all(group.is_alive for group in battle.groups):
        dummy_print_battle(battle=battle, character=character)

        key = input('> ')
        function = key_mapping.get(key)
        if function is not None:
            function()
