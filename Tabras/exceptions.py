class Tabras(Exception):
    pass


class InvalidValue(Tabras):
    pass


class NonEmptyField(Tabras):
    pass


class InvalidAction(Tabras):
    pass
