class Character:
    def __init__(self, battlefield, name=None, alive=True):
        self.field = None
        self.battlefield = battlefield

        self.max_hp = 5
        self.hp = self.max_hp
        self.sight_range = 3
        self.action_points = 2

        self.name = name
        self.alive = alive

    @property
    def range(self):
        return 1

    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'name={self.name}, field={(self.field.x, self.field.y)})')

    def move(self, field):
        self.field = field

    @property
    def fields_in_range(self):
        for field in self.battlefield.fields.range(self.field, self.range,
                                                   empty=True):
            yield field

    def take_damage(self, damage):
        self.hp -= damage
        if self.hp <= 0:
            self.alive = False

    def attack(self, enemy):
        enemy.take_damage(1)


class AICharacter(Character):
    def __init__(self, battlefield):
        super().__init__(battlefield=battlefield, name='Orca')


class PlayerCharacter(Character):
    def __init__(self, battlefield):
        super().__init__(battlefield=battlefield, name='Hooman')
