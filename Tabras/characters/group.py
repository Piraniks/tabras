class CharacterGroup:
    def __init__(self, characters=None, name=None):
        self.characters = characters
        self.name = name

    @property
    def is_alive(self):
        return any(character.alive for character in self.characters)
