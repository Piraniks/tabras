from Tabras.battle.battlefield import Battlefield
from Tabras.characters import PlayerCharacter, CharacterGroup, AICharacter
from Tabras.exceptions import NonEmptyField, InvalidAction


class Battle:
    def __init__(self):
        self.battlefield = Battlefield()
        self.groups = [CharacterGroup([PlayerCharacter(battlefield=self.battlefield)]),
                       CharacterGroup([AICharacter(battlefield=self.battlefield)])]
        for group in self.groups:
            self.battlefield.place_characters(characters=group.characters)

        self.finished = False

    def _move(self, character, field):
        try:
            self.battlefield.move_character(character=character,
                                            field=field)
        except NonEmptyField:
            pass

    def _take_action(self, character, action, field=None):
        if field is not None and field not in character.fields_in_range:
            raise InvalidAction(f'Field {field} is not in range.')

        ...

    def next_turn(self):
        # field = None
        # action = None
        # character = None
        # for character in group:
        #     self._take_action(character=character, action=action, field=field)
        pass

    def run(self):
        while not self.finished:
            self.next_turn()

    # proxies
    @property
    def fields(self):
        return self.battlefield.fields
