from math import sqrt


def distance(field1, field2):
    return sqrt((field1.x - field2.x) ** 2 + (field1.y - field2.y) ** 2)
