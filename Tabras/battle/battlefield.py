from itertools import product

from Tabras import tabras_random
from Tabras.exceptions import InvalidValue, NonEmptyField
from Tabras.utils import none_if_raises
from Tabras.battle.utils import distance


class Field:
    def __init__(self, x, y, content=None):
        self.x = x
        self.y = y
        self.content = content

    def __repr__(self):
        return (f'{self.__class__.__name__}(x={self.x}, y={self.y}, '
                f'content={self.content.name if self.content else None})')


class FieldManager:
    _INVALID_COORDINATES_MESSAGE = 'Position of a field can only be a positive integer. Given: x={x}, y={y}'

    def __init__(self, size):
        self.size = size
        self._fields = [
            [Field(x, y) for x in range(self.size)]
            for y in range(self.size)
        ]

    def get(self, x, y, raises=True):
        if not 0 <= x <= self.size or not 0 <= y <= self.size:
            raise InvalidValue(self._INVALID_COORDINATES_MESSAGE.format(x=x, y=y))

        try:
            return self._fields[y][x]
        except IndexError:
            if raises:
                raise
            return None

    def all(self):
        for row in self._fields:
            for field in row:
                yield field

    def empty(self):
        for field in self.all():
            if field.content is None:
                yield field

    def random(self, empty=False):
        fields = self.empty() if empty else self.all()
        # list for now, later on maybe lazy sequence will be better.
        return tabras_random.choice([field for field in fields])

    def _dimension_range(self, minimum_value, maximum_value):
        real_min = max(minimum_value, 0)
        real_max = min(maximum_value, self.size)

        return range(real_min, real_max)

    def range(self, field, sight_range, empty=False, include=False):
        x_range = self._dimension_range(field.x - sight_range, field.x + sight_range + 1)
        y_range = self._dimension_range(field.y - sight_range, field.y + sight_range + 1)

        for x, y in product(x_range, y_range):
            next_field = self.get(x, y)
            if distance(field, next_field) > sight_range:
                continue

            if empty and next_field.content is not None:
                continue

            if not include and field.x == x and field.y == y:
                continue

            yield next_field

    @none_if_raises(InvalidValue)
    def right(self, field):
        return self.get(field.x + 1, field.y, raises=False)

    @none_if_raises(InvalidValue)
    def left(self, field):
        return self.get(field.x - 1, field.y, raises=False)

    @none_if_raises(InvalidValue)
    def upper(self, field):
        return self.get(field.x, field.y - 1, raises=False)

    @none_if_raises(InvalidValue)
    def lower(self, field):
        return self.get(field.x, field.y + 1, raises=False)


class Battlefield:
    _NONEMPTY_FIELD_MESSAGE = 'Nonempty field was given ({field}). Cannot _move into nonempty field.'

    def __init__(self, size=10):
        self.size = size
        self.fields = FieldManager(size=size)

    def place_characters(self, characters):
        for character in characters:
            field = self.fields.random(empty=True)
            self.move_character(character=character, field=field)

    def move_character(self, character, field):
        if field.content is not None:
            raise NonEmptyField(self._NONEMPTY_FIELD_MESSAGE.format(field=field))

        old_field = character.field
        if old_field is not None:
            old_field.content = None

        character.move(field=field)
        field.content = character

    def visible_fields(self, *characters, include=True):
        fields = set()
        for character in characters:
            for field in self.fields.range(field=character.field,
                                           sight_range=character.sight_range,
                                           include=include):
                if field not in fields:
                    fields.add(field)

        return fields
