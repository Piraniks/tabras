from functools import wraps


class none_if_raises:
    def __init__(self, exception=None):
        self.exception = exception if exception is not None else Exception

    def __call__(self, function):
        @wraps(function)
        def _wrapper(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except self.exception:
                return None

        return _wrapper
